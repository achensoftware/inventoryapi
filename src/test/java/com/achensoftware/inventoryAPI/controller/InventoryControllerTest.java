package com.achensoftware.inventoryAPI.controller;

import com.achensoftware.inventoryAPI.repository.InventoryRepository;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureDataJpa
@AutoConfigureTestDatabase
@AutoConfigureTestEntityManager
@WebMvcTest(InventoryController.class)
@Transactional
class InventoryControllerTest {

    @Resource
    private InventoryRepository repository;

    @Resource
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        repository.deleteAll();
        repository.flush();
    }

    @Test
    public void testShouldReturnAllItems() throws Exception {
        //Given two Inventory Items:
        String inventory1 = buildTestInventoryData(UUID.randomUUID().toString(), "iPhone 10");
        String inventory2 = buildTestInventoryData(UUID.randomUUID().toString(), "iPhone X");
        addInventory(inventory1);
        addInventory(inventory2);

        //That should be retrieved successfully
        mockMvc.perform(get("/inventory"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json("[" + inventory1 + "," + inventory2  + "]"));
    }

    @Test
    public void testShouldReturnAllItems_withOffsetAndLimit() throws Exception {
        //Given the skip is 3 and limit is 2:
        String inventory1 = buildTestInventoryData("A1", "iPhone 10");
        String inventory2 = buildTestInventoryData("A2", "iPhone X");
        String inventory3 = buildTestInventoryData("A3", "iPhone 8");
        String inventory4 = buildTestInventoryData("A4", "iPhone 7");
        String inventory5 = buildTestInventoryData("A5", "iPhone 6");
        String inventory6 = buildTestInventoryData("A6", "iPhone 5");
        addInventory(inventory1);
        addInventory(inventory2);
        addInventory(inventory3);
        addInventory(inventory4);
        addInventory(inventory5);
        addInventory(inventory6);

        //That should only return inventory4 and inventory5
        mockMvc.perform(get("/inventory?skip=3&limit=2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json("[" + inventory4 + "," + inventory3  + "]"));
    }

    @Test
    public void testShouldAddItemAndRetrieveById() throws Exception {
        //Given an Inventory Item with id:
        String inventoryId = UUID.randomUUID().toString();
        String actual = buildTestInventoryData(inventoryId, "iPhone 10");

        addInventory(actual);

        //That should be saved and retrieved successfully
        mockMvc.perform(get("/inventory/{0}", inventoryId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(actual));
    }

    @Test
    public void testShouldReturn409_whenAnItemAlreadyExists() throws Exception {
        //Given an Inventory Item
        String inventoryId = UUID.randomUUID().toString();
        String inventory = buildTestInventoryData(inventoryId, "iPhone 10");
        addInventory(inventory);

        //That should fail when an existing Item already exists
        mockMvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inventory))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    public void testShouldReturn404_whenInventoryNotFound() throws Exception {
        mockMvc.perform(get("/inventory/{0}", "Does not Exist"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testShouldReturn400_whenMandatoryFieldIsMissing() throws Exception {
        mockMvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(buildTestInventoryData("", "iPhone")))
                .andDo(print())
                .andExpect(status().isBadRequest());

        mockMvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(buildTestInventoryData(UUID.randomUUID().toString(), "")))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    private void addInventory(String inventory) throws Exception {
        mockMvc.perform(post("/inventory")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inventory))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    private String buildTestInventoryData(final String id, final String name) {
        return "{\"id\":\"" + id + "\",\"manufacturer\":{\"homePage\":\"http://apple.com\",\"name\":\"Apple\"}," +
                "\"name\":\"" + name + "\",\"releaseDate\":\"2016-08-29T09:12:33.001Z\"}";
    }
}