package com.achensoftware.inventoryAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.achensoftware.inventoryAPI.repository")
@EntityScan(basePackages = "com.achensoftware.inventoryAPI.model")
@ComponentScan(basePackages = "com.achensoftware.inventoryAPI")
@SpringBootApplication
public class InventoryApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventoryApiApplication.class, args);
	}

}
