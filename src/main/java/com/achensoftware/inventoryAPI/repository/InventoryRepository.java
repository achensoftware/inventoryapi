package com.achensoftware.inventoryAPI.repository;

import com.achensoftware.inventoryAPI.model.InventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<InventoryItem, String> {

}
