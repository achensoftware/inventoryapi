package com.achensoftware.inventoryAPI.controller;

import com.achensoftware.inventoryAPI.exception.InventoryNotFoundException;
import com.achensoftware.inventoryAPI.model.InventoryItem;
import com.achensoftware.inventoryAPI.repository.InventoryRepository;
import com.achensoftware.inventoryAPI.repository.OffsetLimitPageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@Validated
public class InventoryController {

    @Resource
    private InventoryRepository inventoryRepository;

    @GetMapping(value = "/inventory", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<InventoryItem> getInventoryItems(
            @RequestParam(required = false, defaultValue = "0") @Min(0)   Integer skip,
            @RequestParam(required = false, defaultValue = "0") @Min(0) @Max(50) Integer limit) {

        if (skip != 0 && limit != 0) {
            OffsetLimitPageable pageable = new OffsetLimitPageable(skip, limit, Sort.by("name").ascending());
            return inventoryRepository.findAll(pageable).getContent();
        }

        return inventoryRepository.findAll();
    }

    @PostMapping(value = "/inventory", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addInventoryItem(@Valid @RequestBody InventoryItem inventoryItem,
                                                   BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        if (inventoryRepository.findById(inventoryItem.getId()).isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }

        inventoryRepository.save(inventoryItem);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @GetMapping(value = "/inventory/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public InventoryItem getInventoryItem(@PathVariable String id) {
        return inventoryRepository.findById(id)
                .orElseThrow(() -> {throw new InventoryNotFoundException();});
    }

    @ExceptionHandler(InventoryNotFoundException.class)
    protected ResponseEntity<String> handleInventoryNotFoundException() {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<String> handle() {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}