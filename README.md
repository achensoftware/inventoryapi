# README #

Inventory API 

* It offers 3 operations:
  * /inventory Get : List available inventory in the system
  * /inventory Post : Adds an item to the system
  * /inventory/{id} Get : Returns an inventory item by id
* Example of an inventory item request:
```json
  {
  	"id": "ssdsdf-sdfsffds-sdfsfsd-sdfsdf",
  	"manufacturer": {
  		"name": "Apple",
  		"homePage": "http://apple.com"
  	},
  	"name": "iPhone 10",
  	"releaseDate": "2016-08-29T09:12:33.001Z"
  }
```

